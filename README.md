# Statistical Analysis of Pairwise Connectivity

This project provides an Open Source implementation for statistical Analysis of pairwise Connectivity in Event streams (ACE) and its evaluation, Reference: ```to be added```. The experiments and ACE itself are implemented in Matlab, while the evaluation, i.e., table and plot generation, are implemented in Python using Jupyter Notebooks.

## Abstract
Analysing correlations between streams of events is an important problem. It arises for example in neurosciences, when the connectivity of neurons should be inferred from spike trains that record neurons' individual spiking activity. While recently some approaches for inferring delayed synaptic connections have been proposed, they are limited in the types of connectivities and delays they are able to handle or require computation-intensive procedures.
This paper proposes a faster and more flexible approach for analysing such delayed correlated activity: a statistical Analysis of pairwise Connectivity in Event streams (ACE) approach, based on the idea of hypothesis testing. It first computes for any pair of a source and a target neuron the inter-spike delays between subsequent source- and target-spikes.
Then, it derives a null model for the distribution of inter-spike delays for uncorrelated neurons. Finally, it compares the observed distribution of inter-spike delays to this null model and infers pairwise connectivity based on the Pearson's Chi-squared test statistic. Thus, ACE is capable to detect connections with a priori unknown, non-discrete (and potentially large) inter-spike delays, which might vary between pairs of neurons. Since ACE works incrementally, it has potential for being used in online processing. In our experiments, we visualise the advantages of ACE in varying experimental scenarios (except for one special case) and in a state-of-the-art dataset which has been generated for neuro-scientific research under most realistic conditions.

# Dependencies
Experiments:  
  * Matlab with configured Mex for C++ compilation (tested with Matlab)  

Evaluation:  
  * Python (tested with 3.7.7)  
  * numpy  
  * pandas  
  * scikit-learn  
  * matplotlib  
  * jupyter-notebook  

# Getting Started
## Prepare Datasets (if not already present):
The artificial datasets used for the experiments are generated via notebooks/dataGeneration/data_generator.ipynb and notebooks/dataGeneration/generate_runtime_datasets.ipynb, which allows for fine control over the spiking characteristics of the simulated neurons. Furthermore, we use a subset of the "sim-1" dataset, which can be found here: http://crcns.org/data-sets/sim/sim-1/about-sim-1/, and is preprocessed using notebooks/dataGeneration/preprocess_sim-1.ipynb.

## Download External Code:
Within this project, we use two libraries, namely Graphical Lasso (http://www.ece.ubc.ca/~xiaohuic/code/glasso/glasso.htm) and Transfer Entropy Toolbox (https://code.google.com/archive/p/transfer-entropy-toolbox/). They can be placed anywhere in the working directory. However, we suggest to download them into ./externalCode/.

## Compile Mex Files:
ACE and the Transfer Entropy Toolbox partly uses C++. The files can be compiled using Mex within Matlab.

``````
cd ace 
mex quantileHistogram.cpp
cd externalCode/transfer-entropy-toolbox-matlab/
mex transent.c
cd ../..
``````

## Experiments
To replicate the Experiments in ```to be added```, cvPrepare20*.m and cvTest_20.m  are used within Matlab. The cvPrepare20*.m scripts executes each strategy on each dataset. The results are aggregated in cvTest_20.m aggregates the results and performs cross-validation. The whole experiment pipeline is as follows:
``````
cvPrepare20;
cvPrepare20_binning_ace;
cvPrepare20_runtime;
cvTest_20;
``````

## Evaluation
The tables and plots presented in ```to be added``` are generated using notebooks/evaluation.ipynb and are either displayed as a cell output (Latex-Code) or exported as .pdf files.

# Results
Achieved F1-scores (incl. standard deviation) on all tested datasets.
|     | ACE                          | IC20                         | IC50                         | HOTE20                       | HOTE50                       |  
|:----|:-----------------------------|:-----------------------------|:-----------------------------|:-----------------------------|:-----------------------------|  
| NU  | 0.7663 (0.08) | 0.2430 (0.11) | 0.1616 (0.07) | 0.6918 (0.14) | 0.6878 (0.14) |  
| LA  | 0.7047 (0.02) | 0.1959 (0.14) | 0.1560 (0.09) | 0.6043 (0.10) | 0.6014 (0.10) |  
| CO  | 0.7650 (0.04) | 0.2376 (0.12) | 0.2013 (0.09) | 0.6882 (0.12) | 0.6851 (0.12) |  
| DE  | 0.4935 (0.28) | 0.3419 (0.04) | 0.1853 (0.05) | 0.4542 (0.25) | 0.5436 (0.15) |  
| NO  | 0.7379 (0.03) | 0.3422 (0.01) | 0.1959 (0.02) | 0.6406 (0.02) | 0.6376 (0.02) |  
| RAT | 0.0947 (0.02) | 0.0595 (0.01) | 0.0658 (0.01) | 0.0066 (0.00) | 0.0284 (0.01) |
