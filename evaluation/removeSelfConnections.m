function [ newConnectionMatrix ] = removeSelfConnections( connectionMatrix )
%REMOVESELFCONNECTIONS Summary of this function goes here
%   Detailed explanation goes here
  id = ~eye(size(connectionMatrix));
  newConnectionMatrix = connectionMatrix.*id;
end

