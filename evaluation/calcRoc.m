function [thresholds, tprValues, fprValues, precisionValues, f1Values, confMatrices ] = calcRoc(probability, realConnectionMatrix , threshHoldValueNum, minThreshold, maxThreshold )
%PLOTROCCURVE Summary of this function goes here
%   Detailed explanation goes here
if ~exist('maxThreshold', 'var')
    maxThreshold = max(probability(:));
end
if ~exist('minThreshold', 'var')
    minThreshold = min(probability(:));
end
thresholdDiff = maxThreshold-minThreshold;
stepWidth = 1/(threshHoldValueNum-3);
thresholds = (((-stepWidth):stepWidth:(1+stepWidth)) * thresholdDiff) + minThreshold;
thresholds = [minThreshold-1 linspace(minThreshold, maxThreshold,threshHoldValueNum-2) maxThreshold+1];
tprValues = zeros(1,threshHoldValueNum);
fprValues = zeros(1,threshHoldValueNum);
precisionValues = zeros(1,threshHoldValueNum);
f1Values = zeros(1,threshHoldValueNum);
confMatrices = cell(1,threshHoldValueNum);
for thresholdIdx = 1:numel(thresholds)
  connection = removeSelfConnections(connectionFromProbability(probability, thresholds(thresholdIdx)));
  confMat = getConfusionMatrix(connection, realConnectionMatrix);
  [recall, precision, fpr, f1] = getStats(confMat);
  tprValues(thresholdIdx) = recall;
  fprValues(thresholdIdx) = fpr;
  precisionValues(thresholdIdx) = precision;
  f1Values(thresholdIdx) = f1;
  confMatrices{thresholdIdx} = confMat;
end
end

