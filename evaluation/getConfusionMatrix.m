function [ confusionMatrix ] = getConfusionMatrix( predictedConnectionMatrix, realConnectionMatrix )
%GETCONFUSIONMATRIX Summary of this function goes here
%   Detailed explanation goes here
idMask = ~eye(size(realConnectionMatrix));
tn = sum(sum((predictedConnectionMatrix == 0) .* (realConnectionMatrix == 0) .* idMask));
fn = sum(sum((predictedConnectionMatrix == 0) .* (realConnectionMatrix == 1) .* idMask));
fp = sum(sum((predictedConnectionMatrix == 1) .* (realConnectionMatrix == 0) .* idMask));
tp = sum(sum((predictedConnectionMatrix == 1) .* (realConnectionMatrix == 1) .* idMask));

confusionMatrix = [tn,fn;fp,tp];

end

