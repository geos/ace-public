function [ auc] = calcAuc( x, y, sorted )
%CALCAUC Summary of this function goes here
%   Detailed explanation goes here

% if  ~exist('sorted', 'var')|| ~sorted
[x, idx] = sort(x, 'ascend');
y = y(idx);
% end
auc = trapz(x, y);

end

