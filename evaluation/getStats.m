function [ recall, precision, fpr, f1] = getStats( confusionMatrix )
%GETSTATS Summary of this function goes here
%   Detailed explanation goes here

tn = confusionMatrix(1,1);
fn = confusionMatrix(1,2);
fp = confusionMatrix(2,1);
tp = confusionMatrix(2,2);

recall = tp/(tp+fn);
if isnan(recall)
  recall = 0;
end
precision = tp/(tp+fp);
if isnan(precision)
  precision = 0;
end
fpr = fp/(fp+tn);
if isnan(fpr)
  fpr = 0;
end
f1 = 2*(precision*recall)/(precision + recall);
if isnan(f1)
  f1 = 0;
end
end

