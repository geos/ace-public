% 2021

datasets = {};
pMats = {};

%generated
folder_path = [fileparts(mfilename('fullpath'))	 '/datasets/runtime/'];
num_bootstraps = 10;
dataset_names = {'50', '100', '150', '200', '250', '300', '350'};
generated_dataset = cell(1, numel(dataset_names));
generated_pmat = cell(1, numel(dataset_names));

for d = 1:numel(dataset_names)
  dataset_name = dataset_names{d};
  generated_dataset_paths = cell(num_bootstraps);
  generated_pmat_paths = cell(num_bootstraps);
  for n = 0:(num_bootstraps - 1)
    dataset_folder_path = [folder_path, dataset_name, '/'];
    generated_dataset_paths{n+1} = [num2str(n), '/', 'stream'];
    generated_pmat_paths{n+1} = [num2str(n), '/', 'pMat'];
  end
  [generated_cases, generated_pmats] = loadDataSetFromPaths( dataset_folder_path, generated_dataset_paths, generated_pmat_paths, false);
  for i = 1:num_bootstraps
    generated_cases{i}(1, :) = generated_cases{i}(1, :) + 1;
    generated_cases{i}(2, :) = generated_cases{i}(2, :);
  end
  datasets = [datasets {generated_cases}];
  pMats = [pMats {generated_pmats}];
end
