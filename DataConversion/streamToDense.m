function [ dense ] = streamToDense( stream, num_neurons )
%STREAMTODENSE Summary of this function goes here
%   Detailed explanation goes here

time  = max(stream(2,:));
spikes = cell(num_neurons, 1);

num_spikes_per_neuron = zeros(num_neurons, 1);
index_per_neuron = ones(num_neurons, 1);

for i = 1:size(stream, 2)
  spike = stream(:,i);
  neuron = spike(1);
%   t = spike(2);
  num_spikes_per_neuron(neuron) = num_spikes_per_neuron(neuron) + 1; 
end

% for i = 1:num_neurons
%   spikes{i} = zeros(num_spikes_per_neuron(neuron), 1);
% end

for i = 1:size(stream, 2)
  spike = stream(:,i);
  neuron = spike(1);
  t = spike(2);
  spikes{neuron}(index_per_neuron(neuron)) = t; 
  index_per_neuron(neuron) = index_per_neuron(neuron) + 1;
end

% construct the dense date from the sparse data
dense = createDense(num_neurons, time, spikes);

end

