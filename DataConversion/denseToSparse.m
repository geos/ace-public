function [ data ] = denseToSparse( dense_data, resolution )
%DENSETOSPARSE Summary of this function goes here
%   Detailed explanation goes here

if ~exist('resolution', 'var')
  resolution = 1;
end

% create a matrix for the whole measurement and set the spikes to 1
data = zeros(dense_data.num_neurons, ceil(dense_data.time * resolution)+1);
for i = 1:dense_data.num_neurons
  data(i,floor(dense_data.spikes{i} * resolution)+1) = data(i,floor(dense_data.spikes{i} * resolution)+1) + 1;
end

end

