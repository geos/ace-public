function [ data ] = asdfToDense( asdf )
%ASDFTODENSE Summary of this function goes here
%   Detailed explanation goes here

% extract the information out of the cell array, which is in asdf format
spikes = asdf(1:end-2);
num_neurons = asdf{end}(1);
time = asdf{end}(2);
data = createDense(num_neurons, time, spikes);
end

