function [ data ] = denseToAsdf( dense_data)
%DENSETOSPARSE Summary of this function goes here
%   Detailed explanation goes here

% put all the information into one cell array to match the asdf format
spikes = dense_data.spikes;
for i = 1:length(spikes)
  spikes{i} = round(spikes{i});
end
data = spikes;
data{end + 1} = 1;
data{end + 1} = [dense_data.num_neurons, dense_data.time];
end

