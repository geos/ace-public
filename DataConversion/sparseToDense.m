function [ data ] = sparseToDense( sparse_data )
%SPARSETOLIST Summary of this function goes here
%   Detailed explanation goes here

% get the number of neurons and the duration of the measurement
[num_neurons, time] = size(sparse_data);

% get all the spikes 
[neurons, spike_times] = find(sparse_data);
% save the spikes for each neuron in seperate lists
spikes = cell(num_neurons, 1);
for i = 1:length(spike_times)
  spikes{neurons(i)}(end+1) = spike_times(i);
end

% construct the dense date from the sparse data
data = createDense(num_neurons, time, spikes);

end