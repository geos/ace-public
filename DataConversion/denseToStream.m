function [ stream ] = denseToStream( dense )
%DENSETOSTREAM Summary of this function goes here
%   Detailed explanation goes here

% count total number of spikes over all neurons
num_data = 0;
for i = 1:dense.num_neurons
  num_data = num_data + length(dense.spikes{i});
end

% put all data into one single matrix
stream = zeros(num_data, 2);
cur_index = 1;
for i = 1:dense.num_neurons
  num_spikes = length(dense.spikes{i});
  % set timestamp
  stream(cur_index:(cur_index + num_spikes - 1),1) = dense.spikes{i};
  % set neuron index
  stream(cur_index:(cur_index + num_spikes - 1),2) = i;
  cur_index = cur_index + num_spikes;
end

% sort the spikes by time
stream = sortrows(stream)';
stream = [stream(2,:); stream(1,:)];
end

