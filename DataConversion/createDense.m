function [ data ] = createDense( num_neurons, time, dense_spike_data )
%CREATEDENSE Summary of this function goes here
%   Detailed explanation goes here

% put the data into a single struct
data = struct();
data.num_neurons = num_neurons;
data.time = time;
data.spikes = dense_spike_data;
end

