function denseToSplittedTxt( dense, folderPath)
    if ~exist(folderPath, 'dir')
        mkdir(folderPath);
    end
    num_neurons = dense.num_neurons;
    sum_spikes = sum(cellfun(@(x) numel(x), dense.spikes));
    for i = 1:num_neurons
        neuron_path = strcat(folderPath, '/', num2str(i), '.txt');     
        fileID = fopen(neuron_path,'w');
        fprintf(fileID,'  %d\n',sum_spikes);
        fprintf(fileID,'    %d\n',numel(dense.spikes{i}));
        fprintf(fileID,'      %d\n',dense.spikes{i}*10);   
        fclose(fileID);
    end
end