addpath(genpath(pwd));
exportFolder = 'export_cv/';
exportFolderContent = dir(exportFolder);
exportFolderContent = exportFolderContent(3:end);
strategyFolder = {exportFolderContent([exportFolderContent.isdir]).name};

for strategyIdx = 1:length(strategyFolder)
  strategyName = strategyFolder{strategyIdx};
  strategyPath = [exportFolder strategyName '/'];
  strategyFolderContent = dir(strategyPath);
  strategyFolderContent = strategyFolderContent(3:end);
  datasetFolder = {strategyFolderContent([strategyFolderContent.isdir]).name};
  for datasetIdx = 1:length(datasetFolder)
    datasetName = datasetFolder{datasetIdx};
    datasetPath = [strategyPath datasetName '/result.mat'];
    resultFile = load(datasetPath);
    result = resultFile.data;
    foldData = repmat(struct( 'f1Pos', 0, ...
                              'threshold', 0, ...
                              'transformedThreshold', 0, ...
                              'realConnectionMatrix', 0, ...
                              'predictedConnectionMatrix', 0, ...
                              'resultMatrix', 0, ...
                              'confMatrix', 0, ...
                              'recall', 0, ...
                              'precision', 0, ...
                              'fpr', 0, ...
                              'f1', 0, ...
                              'auc', 0, ...
                              'auprc', 0, ...
                              'time', 0), size(result));
                     
    disp(datasetPath);
    minThreshold = inf;
    maxThreshold = -inf;
    for foldIdx = 1:length(result)
        minThreshold = min(minThreshold, min(result(foldIdx).resultMatrix(:)));
        maxThreshold = max(maxThreshold, max(result(foldIdx).resultMatrix(:)));
    end
    
    foldDataResult = repmat(struct('thresholds', [], 'f1Values', [], 'confMatrices', []),3);
    for foldIdx = 1:length(result)
        numThresholds = 10000;
        [thresholds, tprValues, fprValues, precisionValues, f1Values, confMatrices ] = calcRoc(result(foldIdx).resultMatrix, result(foldIdx).connectionMatrix , numThresholds, minThreshold, maxThreshold);        
        foldDataResult(foldIdx).thresholds = thresholds;
        foldDataResult(foldIdx).f1Values = f1Values;
        foldDataResult(foldIdx).confMatrices = confMatrices;
    end
    
    
    for foldIdx = 1:length(result)    
      foldF1Values = cat(1,foldDataResult.f1Values);
      foldF1Values(isnan(foldF1Values)) = 0;
      foldF1Values(foldIdx,:) = [];
      meanF1Values = mean(foldF1Values, 1);
%       maxF1Pos = round(mean(find(meanF1Values == max(meanF1Values))));
      [maxF1,maxF1Pos] = max(meanF1Values);
      foldResultMatrix = result(foldIdx).resultMatrix;
      foldConnectionMatrix = result(foldIdx).connectionMatrix>0;
      foldThresholds = foldDataResult(foldIdx).thresholds;
      foldThreshold = foldThresholds(maxF1Pos);
      
      stepWidth = 1/(length(foldThresholds)-3);
      transformedThresholds = (((-stepWidth):stepWidth:(1+stepWidth)) * 1);
      transformedThreshold = transformedThresholds(maxF1Pos);
      
      connection = removeSelfConnections(connectionFromProbability(foldResultMatrix, foldThreshold));
      confMatrix = getConfusionMatrix(connection, foldConnectionMatrix);
      
      auc = result(foldIdx).auc;
      auprc = result(foldIdx).auprc;
      
      [recall, precision, fpr, f1] = getStats(confMatrix);
      foldData(foldIdx).auc = auc;
      foldData(foldIdx).auprc = auprc;
      foldData(foldIdx).f1Pos = maxF1Pos;
      foldData(foldIdx).threshold = foldThreshold;
      foldData(foldIdx).transformedThreshold = transformedThreshold;
      foldData(foldIdx).realConnectionMatrix = foldConnectionMatrix;
      foldData(foldIdx).predictedConnectionMatrix = connection;
      foldData(foldIdx).resultMatrix = foldResultMatrix;
      foldData(foldIdx).confMatrix = confMatrix;
      foldData(foldIdx).recall = recall;
      foldData(foldIdx).precision = precision;
      foldData(foldIdx).fpr = fpr;
      foldData(foldIdx).f1 = f1;
      foldData(foldIdx).thresholds = foldDataResult(foldIdx).thresholds;
      foldData(foldIdx).f1Values = foldDataResult(foldIdx).f1Values;
      foldData(foldIdx).time = sum(cat(1, result.time));
%       disp(['Fold: ' num2str(foldIdx)]);
%       disp(foldData(foldIdx));
    end
    
    meanData = struct();
    fieldNames = fieldnames(foldData);
    for fieldIdx = 1:length(fieldNames)
      fieldName = fieldNames{fieldIdx};
      try
        concatDim = ndims(foldData(1).(fieldName))+1;
        concatenatedFieldData = cat(concatDim, foldData.(fieldName));
        meanData.(fieldName) = mean(concatenatedFieldData, concatDim);
      catch ME
        meanData.(fieldName) = [];
      end
    end
    meanDispData = struct();
    meanDispData.f1 = meanData.f1;
    meanDispData.auc = meanData.auc;
    meanDispData.auprc = meanData.auprc;
%     meanDispData.tn = meanData.confMatrix(1,1);
%     meanDispData.fn = meanData.confMatrix(1,2);
%     meanDispData.fp = meanData.confMatrix(2,1);
%     meanDispData.tp = meanData.confMatrix(2,2);
    meanDispData.time = meanData.time;
    
    disp(meanDispData)
    
    foldResultCVPath = [strategyPath datasetName '/resultCV.mat'];
    save(foldResultCVPath , 'foldData');
    
    meanResultCVPath = [strategyPath datasetName '/resultMeanCV.mat'];
    save(meanResultCVPath , 'meanData');
  end
end