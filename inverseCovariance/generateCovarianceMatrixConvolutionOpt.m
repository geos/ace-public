function [s, convolvedData] = generateCovarianceMatrixConvolutionOpt( sparse, kernel)
%CALC_COVARIANCE_MATRIX Summary of this function goes here
%   Detailed explanation goes here
%   [numNeurons, time] = size(sparse);
  convolvedData = conv2(1, kernel,sparse,'same');
  s=cov(convolvedData');
end

