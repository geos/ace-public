function [probabilities] = nullpdf(rp, p, latencies)
probabilities = zeros(size(latencies));

smaller = latencies < rp;
smallerIndices = find(smaller);
biggerIndices = 1:length(latencies);
biggerIndices(smallerIndices) = [];

probabilities(smallerIndices) = 1;
probabilities(biggerIndices) = exp(-p * (latencies(biggerIndices) - rp)); %(p/(1+rp*p)) * (1-p).^(latencies(biggerIndices)-rp-1);
probabilities = probabilities/(rp+(1/p));
end

