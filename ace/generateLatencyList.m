function [L] = generateLatencyList( dense, resolutionFactor )
%GENERATEHISTOGRAM Summary of this function goes here
%   Detailed explanation goes here

if ~exist('resolutionFactor', 'var')
  resolutionFactor = 1;
end

stream = denseToStream(dense);
r = zeros(dense.num_neurons, 1);
L = cell(dense.num_neurons);
max_latency = 0;
% h = cell(dense.num_neurons);

last_t = -1;
r_cur = [];
it = 0;
for d = stream
  t = d(1);
  k = d(2);
  
  if last_t ~= t
    for d2 = r_cur
      t2 = d2(1);
      k2 = d2(2);
      r(k2) = t2;
    end
    r_cur = [];
    last_t = t;
  end
  
  for i = 1:dense.num_neurons
    if r(i) > 0
      lsk = floor((t - r(i))*resolutionFactor);
      max_latency = max(max_latency, lsk);
      L{i,k} = [L{i,k} lsk]; 
    end
  end
  
%   r(k) = t;
  r_cur(:,size(r_cur, 2) + 1) = [t; k];
%   if mod(it, 10) == 0
%     disp([num2str(it) '/' num2str(length(stream))]);
%   end
  it = it + 1;
end
% for i = 1:dense.num_neurons
%   for k = 1:dense.num_neurons
%     h{i,k} = hist(L{i,k}, 0:max_latency);
%   end
% end
end

