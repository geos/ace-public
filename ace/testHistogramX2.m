function [ result ] = testHistogramX2( h )
numQuantiles = size(h,3);
expected = repmat(sum(h, 3)/numQuantiles, [1, 1, numQuantiles]);
result = sum(((h - expected).^2)./expected, 3);
end