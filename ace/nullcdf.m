function [cdf] = nullcdf(rp, p, latencies)
  part1 = latencies./(rp+1/p) .* (1*latencies<rp);
  part2 = (rp/(rp+1/p) + (1-exp(-p*(latencies-rp)))/(p*rp+1)) .* (1*latencies>=rp);
  cdf = part1+part2;
end