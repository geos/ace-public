function [ quantileLocation ] = calcUniQuantile(rp, p, numQuantiles )

quantiles = 0:(1/(numQuantiles)):(numQuantiles-1)/(numQuantiles);

smallerEqual = quantiles < rp/(rp + 1/p);
smallerIndices = find(smallerEqual);
biggerIndices = 1:length(quantiles);
biggerIndices(smallerIndices) = [];

quantileLocation = zeros(size(quantiles));
quantileLocation(smallerIndices) = quantiles(smallerIndices) * (rp + 1/p);
quantileLocation(biggerIndices) = rp - log(1 - (quantiles(biggerIndices)-rp/(rp+1/p))*(p*rp+1))/p;

end

