function [ quantileLocations, p, rp ] = generateQuantileLocationsFromDense( dense, numQuantiles)
latencyListPerNeuron = cell(dense.num_neurons, 1);

for n = 1:dense.num_neurons
  lastSpikeTime =  dense.spikes{n}(1:end-1);
  curSpikeTime = dense.spikes{n}(2:end);
  latencyListPerNeuron{n} = curSpikeTime-lastSpikeTime;
end

[~, ~, p, rp] = getNullmodelFromLatencylist(latencyListPerNeuron);
quantileLocations = zeros(dense.num_neurons,numQuantiles);
for i = 1:dense.num_neurons
    quantileLocations(i,:) = calcUniQuantile(rp(i), p(i), numQuantiles );
end


end

