function [hist  ] = generateQuantileHistogram( dense, quantileLocations )
%GENERATEQUANTILEHISTOGRAM Summary of this function goes here
%   Detailed explanation goes here
stream = denseToStream(dense);

hist = quantileHistogram(stream, quantileLocations);
hist = reshape(hist, [dense.num_neurons,dense.num_neurons,size(quantileLocations, 2)]);
end

