function [ meanLatency, varLatency, p, rp ] = getNullmodelFromLatencylist( latencyList )

meanLatency = cellfun(@mean, latencyList);
varLatency = cellfun(@var, latencyList);
p = 1./sqrt(varLatency);
rp = meanLatency-(1./p);
end