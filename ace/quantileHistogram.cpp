#include "mex.h"
#include <iostream>

// unisgned int getHistIndex(unsigned int s, unsigned int k, unsigned int q, mwSize numNeurons)
// {
//   return s + k * numNeurons + q * numNeurons * numNeurons;
// }

class mystream : public std::streambuf
{
protected:
  virtual std::streamsize xsputn(const char *s, std::streamsize n) { mexPrintf("%.*s", n, s); return n; }
  virtual int overflow(int c=EOF) { if (c != EOF) { mexPrintf("%.1s", &c); } return 1; }
};
class scoped_redirect_cout
{
public:
	scoped_redirect_cout() { old_buf = std::cout.rdbuf(); std::cout.rdbuf(&mout); }
	~scoped_redirect_cout() { std::cout.rdbuf(old_buf); }
private:
	mystream mout;
	std::streambuf *old_buf;
};
static scoped_redirect_cout mycout_redirect;


/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
  /* variable declarations here */
  double *stream = mxGetPr(prhs[0]);
  mwSize  numSpikes = mxGetDimensions(prhs[0])[1];
  double *quantileLocations = mxGetPr(prhs[1]);
  const mwSize *quantileLocationsDim = mxGetDimensions(prhs[1]);
  mwSize numNeurons = quantileLocationsDim[0];
  mwSize numQuantiles = quantileLocationsDim[1];

  mwSize histSize = numNeurons * numNeurons * numQuantiles;
  plhs[0] = mxCreateDoubleMatrix(1,histSize,mxREAL);
  double *outMatrix = mxGetPr(plhs[0]);
  /* code here */
  
  double *r = new double[numNeurons];
  for(int i = 0; i < numNeurons; ++i)
  {
    r[i] = 0.0;
  }
  
  for(int spikeIndex = 0; spikeIndex < numSpikes; ++spikeIndex)
  {
    double t = stream[spikeIndex * 2 + 1];
    
    unsigned int k = (unsigned int)(stream[spikeIndex * 2]) - 1;
    
    for(int s = 0; s < numNeurons; ++s)
    {
      double lsk = t - r[s];
      unsigned int index = numQuantiles - 1;
      while(index > 0 && quantileLocations[s + (index)*numNeurons] > lsk)
      {
        --index;
      }
      unsigned int histIndex = s + k * numNeurons + index * numNeurons * numNeurons;
      outMatrix[histIndex] +=1;
    }
    r[k] = t;
  }
  delete [] r;
}