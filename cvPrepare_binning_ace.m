addpath(genpath(pwd));
loadDataSetBinning;
% loadHistograms;

numDatasets = length(datasets);

numQuantilesToTest = 10:10:200;

aceTemplateStrategy = createStrategyStruct('ace_x2', 'ace_x2', struct('numQuantiles', 1000));
aceStrategies = repmat(aceTemplateStrategy , 1, length(numQuantilesToTest));
for i = 1:length(numQuantilesToTest)
  aceStrategies(i).name = ['x2_17_' num2str(numQuantilesToTest(i))];
  aceStrategies(i).param = struct('numQuantiles', numQuantilesToTest(i));
end

numThresholds = 10000;


strategies = aceStrategies;

datasetExportPaths = {'connections'};

disp('created strategies');

for datasetIndex = 1:length(datasets)
  cases = datasets{datasetIndex};
  connectionMatrices = pMats{datasetIndex};
  numDatasets = length(cases);
  for i = 1:length(strategies)
    strategy = strategies(i);
    data = repmat(struct(...
      'strategyName', '', ...
      'thresholds', [], ...
      'tprValues', [], ...
      'fprValues', [], ...
      'precisionValues', [], ...
      'f1Values', [], ...
      'confMatrices', [], ...
      'auc', 0, ...
      'auprc', 0, ...
      'time', 0, ...
      'resultMatrix', [], ...
      'connectionMatrix', []), ...
      1, length(numDatasets));
    
      for caseIndex = 1:numDatasets
        caseIndex
        realConnectionMatrix = connectionMatrices{caseIndex};
        num_neurons = length(realConnectionMatrix);
        dense = streamToDense(cases{caseIndex}, num_neurons);
        asdf = denseToAsdf(dense);
        sparse = denseToSparse(dense, 0.1);
        data(caseIndex) = testStrategy( strategies(i), cases{caseIndex}, sparse, dense, asdf, realConnectionMatrix, numThresholds );
      end


      exportDir = ['export_cv_binning/' strategies(i).name '/' datasetExportPaths{datasetIndex} '/'];
      if ~exist(exportDir, 'dir')
        mkdir(exportDir);
      end
      save([exportDir 'result'], 'data');
      disp(['exported to ' [exportDir 'result']]);
  end
end
