datasets = {};
pMats = {};

% sim-1
subsampledNet_CasePaths = cell(1,3);
subsampledNet_PMatPaths = cell(1,3);
datasetFolder = [fileparts(mfilename('fullpath'))	 '/datasets/sim1_subset/'];
for i = 1:3
  subsampledNet_CasePaths{i} = [num2str(i-1) '/' 'stream'];
  subsampledNet_PMatPaths{i} = [num2str(i-1) '/' 'pMat'];
end
[subsampledNet_Cases, subsampledNet_PMats] = loadDataSetFromPaths( datasetFolder, subsampledNet_CasePaths, subsampledNet_PMatPaths, false);
for i = 1:3
  subsampledNet_Cases{i}(1, :) = subsampledNet_Cases{i}(1, :) + 1;
  subsampledNet_Cases{i}(2, :) = subsampledNet_Cases{i}(2, :) * 1000;
end
datasets = [datasets {subsampledNet_Cases}];
pMats = [pMats {subsampledNet_PMats}];



% connetomics
connectomics_CasePaths = cell(1,4);
connectomics_PMatPaths = cell(1,4);
datasetFolder = [fileparts(mfilename('fullpath'))	 '/datasets/connectomics/'];
for i = 1:4
  connectomics_CasePaths{i} = [num2str(i-1) '/' 'stream'];
  connectomics_PMatPaths{i} = [num2str(i-1) '/' 'pMat'];
end
[connectomics_Cases, connectomics_PMats] = loadDataSetFromPaths( datasetFolder, connectomics_CasePaths, connectomics_PMatPaths, true);
for i = 1:4
  connectomics_Cases{i}(1, :) = connectomics_Cases{i}(1, :) + 1;
  connectomics_Cases{i}(2, :) = connectomics_Cases{i}(2, :) * 1000;
end
datasets = [datasets {connectomics_Cases}];
pMats = [pMats {connectomics_PMats}];



% connetomics_remove_repeated
connectomics_rr_CasePaths = cell(1,4);
connectomics_rr_PMatPaths = cell(1,4);
datasetFolder = [fileparts(mfilename('fullpath'))	 '/datasets/connectomics_removed_repeated/'];
for i = 1:4
  connectomics_rr_CasePaths{i} = [num2str(i-1) '/' 'stream'];
  connectomics_rr_PMatPaths{i} = [num2str(i-1) '/' 'pMat'];
end
[connectomics_rr_Cases, connectomics_rr_PMats] = loadDataSetFromPaths( datasetFolder, connectomics_rr_CasePaths, connectomics_rr_PMatPaths, true);
for i = 1:4
  connectomics_rr_Cases{i}(1, :) = connectomics_rr_Cases{i}(1, :) + 1;
  connectomics_rr_Cases{i}(2, :) = connectomics_rr_Cases{i}(2, :) * 1000;
end
datasets = [datasets {connectomics_rr_Cases}];
pMats = [pMats {connectomics_rr_PMats}];

% %generated
folder_path = [fileparts(mfilename('fullpath'))	 '/datasets/generated/'];
num_bootstraps = 3;
dataset_names = {'connections', 'delay', 'noisy', 'neurons', 'expectedLatency'};
generated_dataset = cell(1, numel(dataset_names));
generated_pmat = cell(1, numel(dataset_names));

for d = 1:numel(dataset_names)
  dataset_name = dataset_names{d};
  generated_dataset_paths = cell(num_bootstraps);
  generated_pmat_paths = cell(num_bootstraps);
  for n = 0:(num_bootstraps - 1)
    dataset_folder_path = [folder_path, dataset_name, '/'];
    generated_dataset_paths{n+1} = [num2str(n), '/', 'stream'];
    generated_pmat_paths{n+1} = [num2str(n), '/', 'pMat'];
  end
  [generated_cases, generated_pmats] = loadDataSetFromPaths( dataset_folder_path, generated_dataset_paths, generated_pmat_paths, false);
  for i = 1:num_bootstraps
    generated_cases{i}(1, :) = generated_cases{i}(1, :) + 1;
    generated_cases{i}(2, :) = generated_cases{i}(2, :);
  end
  datasets = [datasets {generated_cases}];
  pMats = [pMats {generated_pmats}];
end