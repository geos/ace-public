function [ datasets, pMats ] = loadDataSetFromPaths( datasetFolder, datasetNames, pMatNames, transposeDataSet )
%LOADDATASETFROMPATHS Summary of this function goes here
%   Detailed explanation goes here

datasets = cell(1, length(datasetNames));
pMats = cell(1, length(pMatNames));

for i = 1:length(datasetNames)
  dataset = load([datasetFolder datasetNames{i}]);
  pMat = load([datasetFolder pMatNames{i}]);
  datasets{i} = getStructContent(dataset);
  if transposeDataSet
    datasets{i} = datasets{i}';
  end
  
  datasets{i} = sortrows(datasets{i}',2)';
  
  pMats{i} = getStructContent(pMat);
end

end

function [ content ] = getStructContent(s)
  fields = fieldnames(s);
  content = s.(fields{1});
end