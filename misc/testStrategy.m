function [ data ] = testStrategy( strategy, stream, sparse, dense, asdf, connectionMatrix, numThresholds )
%TESTSTRATEGY Summary of this function goes here
%   Detailed explanation goes here

defaultNumQantiles = 100;

resultMatrix = [];
startTime = tic;

switch strategy.type
  case 'ace_x2'
    numQuantiles = defaultNumQantiles;
    if isfield(strategy.param, 'numQuantiles')
        numQuantiles = strategy.param.numQuantiles;
    end
    quantileLocation = generateQuantileLocationsFromDense(dense, numQuantiles);
    histogram = generateQuantileHistogram( dense, quantileLocation );
    resultMatrix = testHistogramX2(histogram);
  case 'ic'
    covMatrix = generateCovarianceMatrixConvolutionOpt(sparse, strategy.param.kernel);
    [~, resultMatrix] = graphicalLasso(covMatrix, strategy.param.rho);
  case 'te'
%     resultMatrix = ASDFTE_parallel(asdf, strategy.param.ds, strategy.param.i_order, strategy.param.j_order, strategy.param.window);
    resultMatrix = ASDFTE(asdf, strategy.param.ds, strategy.param.i_order, strategy.param.j_order, strategy.param.window);
end
time = toc(startTime);

[thresholds, tprValues, fprValues, precisionValues, f1Values, confMatrices ] = calcRoc(resultMatrix, connectionMatrix , numThresholds );
data = struct();
data.strategyName = strategy.name;
data.thresholds = thresholds;
data.tprValues = tprValues;
data.fprValues = fprValues;
data.precisionValues = precisionValues;
data.f1Values = f1Values;
data.confMatrices = confMatrices;
data.auc = calcAuc(fprValues, tprValues);
data.auprc = calcAuc(tprValues, precisionValues);
data.time = time;
data.resultMatrix = resultMatrix;
data.connectionMatrix = connectionMatrix;

end

