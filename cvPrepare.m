addpath(genpath(pwd));
loadDataSet;
% loadHistograms;

numDatasets = length(datasets);


kernelSizes = 10:10:100;
rho = 0;

aceX2Strategy = createStrategyStruct('ace_x2', 'ace_x2', struct());

icStrategies = repmat(createStrategyStruct('ic', 'ic', struct()), 1, length(kernelSizes));
for i = 1:length(kernelSizes)
  icStrategies(i).name = ['ic_' num2str(kernelSizes(i))];
  icStrategies(i).param = struct('kernel', ones(1,kernelSizes(i)),'rho', rho);
end

ds = 10:10:100;
order = 1;
numParams = length(ds);
window = 5;
itoParams = repmat(struct('ds', 0, 'i_order', 0, 'j_order', 0, 'window', window), [1, numParams]);
[X,Y,Z] = meshgrid(1:length(ds), 1:length(order), 1:length(order));



for i = 1:numParams
  itoParams(i).ds = 1:ds(X(i));
  itoParams(i).i_order = order(Y(i));
  itoParams(i).j_order = order(Z(i));
end

itoStrategies = repmat(createStrategyStruct('te', 'te', struct()), 1, length(itoParams));
for i = 1:length(itoParams)
  itoStrategies(i).name = ['te_' num2str(itoParams(i).ds(end)) '_' num2str(itoParams(i).i_order) '_' num2str(itoParams(i).j_order)];
  itoStrategies(i).param = itoParams(i);
end

ic20 = createStrategyStruct('ic', 'ic', struct());
ic20.name = ['ic_' num2str(20)];
ic20.param = struct('kernel', ones(1,20),'rho', rho);
ic50 = createStrategyStruct('ic', 'ic', struct());
ic50.name = ['ic_' num2str(50)];
ic50.param = struct('kernel', ones(1,50),'rho', rho);

itoTe50_2_2 = createStrategyStruct('te', 'te', struct());
itoTe50_2_2.name = ['te_' num2str(50) '_' num2str(2) '_' num2str(2)];
itoTe50_2_2.param = struct('ds', 1:50, 'i_order', 2, 'j_order', 2, 'window', 5);
itoTe20_2_2 = createStrategyStruct('te', 'te', struct());
itoTe20_2_2.name = ['te_' num2str(20) '_' num2str(2) '_' num2str(2)];
itoTe20_2_2.param = struct('ds', 1:20, 'i_order', 2, 'j_order', 2, 'window', 5);

numThresholds = 10000;

strategies = [aceX2Strategy, ic20, ic50, itoTe20_2_2, itoTe50_2_2];

datasetExportPaths = {'sim1_subset', 'connectomics', 'connectomics_remove_repeated', 'connections', 'delay', 'noisy', 'neurons', 'expectedLatency'};

disp('created strategies');

% for datasetIndex = 1:length(datasets)
for datasetIndex = [4]
  cases = datasets{datasetIndex};
  connectionMatrices = pMats{datasetIndex};
  numDatasets = length(cases);
  for i = 1:length(strategies)
    strategy = strategies(i);
    data = repmat(struct(...
      'strategyName', '', ...
      'thresholds', [], ...
      'tprValues', [], ...
      'fprValues', [], ...
      'precisionValues', [], ...
      'f1Values', [], ...
      'confMatrices', [], ...
      'auc', 0, ...
      'auprc', 0, ...
      'time', 0, ...
      'resultMatrix', [], ...
      'connectionMatrix', []), ...
      1, length(numDatasets));
    
      for caseIndex = 1:numDatasets
        caseIndex
        realConnectionMatrix = connectionMatrices{caseIndex};
        num_neurons = length(realConnectionMatrix);
        dense = streamToDense(cases{caseIndex}, num_neurons);
        asdf = denseToAsdf(dense);
        sparse = denseToSparse(dense, 0.01);
        data(caseIndex) = testStrategy( strategies(i), cases{caseIndex}, sparse, dense, asdf, realConnectionMatrix, numThresholds );
      end


      exportDir = ['export_cv/' strategies(i).name '/' datasetExportPaths{datasetIndex} '/'];
      if ~exist(exportDir, 'dir')
        mkdir(exportDir);
      end
      save([exportDir 'result'], 'data');
      disp(['exported to ' [exportDir 'result']]);
  end
end
